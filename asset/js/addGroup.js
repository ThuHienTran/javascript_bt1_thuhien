  
    var $ = function(id) {
      return document.getElementById(id);
    }

    function createGroup() {
      
      var isValid = true;

      var groupNameValue = $('group-name');
      var groupNameRequired = $('group-name-required');
      if (groupNameValue.value.length === 0) {
        groupNameRequired.innerHTML = "*";
        isValid = false;
      } 
      else {
        groupNameRequired.innerHTML = "";
      }

      var separateRadioValue = $('separate-group');
      var otherValue = $('other');
      var radioValue1 = $('radio-required-1');
      var radioValue2 = $('radio-required-2');
      if (separateRadioValue.checked !==true && otherValue.checked !==true) {
        radioValue1.innerHTML= 'Select the option for new group';
        radioValue2.innerHTML = 'Select the option for new group';
        isValid = false;
      }
      else{
        radioValue1.innerHTML= '';
        radioValue2.innerHTML = '';
      }
      var categoryValue = $('category');
      var categoryRequired = $('category-required');
      if (categoryValue.value === '0') {
        categoryRequired.innerHTML = "*";
        isValid = false;
      } 
      else {
        categoryRequired.innerHTML = "";
        categoryRequired.style.border = "red";
      }
      if (isValid) {
        console.log('Bạn đã đăng kí thành công');
      }
    }
    var submitButton = $('create');
    submitButton.onclick = createGroup;